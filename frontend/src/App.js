import './App.css';
import Login from "./Login";
import {useState} from "react";
import Chat from "./Chat";

function App() {
  const [user, setUser] = useState();

  const handleLogin = (user) => {
    setUser(user);
  }

  const handleLogout = () => {
    setUser(undefined);
  }

  if (!user) {
    return <Login onLogin={handleLogin}/>;
  }

  return (
    <>
      <Chat user={user} />
      <button onClick={handleLogout}>Logout</button>
    </>
  )
}

export default App;
