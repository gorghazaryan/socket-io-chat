import React from 'react';

const Message = ({ message }) => {
  const { body, user: { gender, username } } = message;

  return (
    <div>
      <strong style={{
        color: gender === "male" ? "blue" : gender === "female" ? "green" : "black"
      }}>{username}</strong>: {" "}
      {body}
    </div>
  );
};

export default Message;