import React, {useState} from 'react';

const Login = ({ onLogin }) => {
  const [formData, setFormData] = useState({
    username: "",
    gender: ""
  });

  const handleChange = name => event => {
    setFormData(formData => ({
      ...formData,
      [name]: event.target.value,
    }))
  }

  const handleSubmit = (event) => {
    event.preventDefault();
    onLogin(formData);
  }

  return (
    <form onSubmit={handleSubmit}>
      <label>
        Username
        <input type={"text"} value={formData.username} onChange={handleChange("username")} />
      </label>
      <label>
        Gender
        <select value={formData.gender} onChange={handleChange("gender")}>
          <option value={""}>-</option>
          <option value={"male"}>Male</option>
          <option value={"female"}>Female</option>
        </select>
      </label>
      <button>Login</button>
    </form>
  );
};

export default Login;