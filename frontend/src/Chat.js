import React, {useEffect, useRef, useState} from 'react';
import { io } from "socket.io-client";
import Message from "./Message";

const Chat = ({ user }) => {
  const [message, setMessage] = useState("");
  const [messages, setMessages] = useState([]);

  const socket = useRef();

  const handleChange = event => {
    setMessage(event.target.value);
  }

  const handleSubmit = event => {
    event.preventDefault();

    socket.current.emit("send-message", message);
    setMessage("");
  }

  useEffect(() => {
    socket.current = io("http://localhost:4000");

    socket.current.on("connect", () => {
      socket.current.emit("connected", user);
    });

    socket.current.on("user-message", message => {
      setMessages(messages => ([...messages, message]));
    });

    return () => {
      socket.current.disconnect();
    }
  }, []);

  return (
    <div>
      <div className="chat-container">
        {messages.map((message, index) => {
          return <Message message={message} key={index} />
        })}
      </div>
      <form onSubmit={handleSubmit}>
        <input type={"text"} value={message} onChange={handleChange} />
        <button>Send</button>
      </form>
    </div>
  );
};

export default Chat;