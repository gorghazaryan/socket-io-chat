const { createServer } = require("http");
const { Server } = require("socket.io");

const httpServer = createServer();
const io = new Server(httpServer, {
  cors: {
    origin: "http://localhost:3000",
  }
});

const users = new Map();

io.on("connection", (socket) => {
  console.log("Someone connected");

  socket.on("connected", user => {
    users.set(socket.id, user);
    console.log(`User ${user.username} is connected`);
  });

  socket.on("disconnect", () => {
    const user = users.get(socket.id);
    console.log(`User ${user.username} is disconnected`);
    users.delete(socket.id);
  });

  socket.on("send-message", message => {
    const user = users.get(socket.id);
    console.log(`User ${user.username} sent message ${message}`);
    io.emit("user-message", {
      body: message,
      user: users.get(socket.id),
    });
  });
});

httpServer.listen(4000);